package com.prueba.todolist.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.prueba.todolist.builders.ComposeTaskBuilder;
import com.prueba.todolist.constants.Constants;
import com.prueba.todolist.domain.Task;
import com.prueba.todolist.input.RequestTaskInput;
import com.prueba.todolist.service.TaskService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class CreateTaskCmd implements BusinessLogicCommand {
    private TaskService service;

    @Setter
    private RequestTaskInput request;

    @Getter
    private Task response;

    public CreateTaskCmd(TaskService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        create();
    }

    private void create() {
        try {
            response = createTask();
        }catch (Exception e){
            throw  new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private Task createTask() {
        return service.createTask(ComposeTaskBuilder.composeTask(request));
    }
}
