package com.prueba.todolist.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.prueba.todolist.constants.Constants;
import com.prueba.todolist.input.ChangePriorityTaskInput;
import com.prueba.todolist.service.TaskService;
import jakarta.persistence.EntityNotFoundException;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class ChangePriorityTaskCmd implements BusinessLogicCommand {

    private TaskService service;

    @Setter
    private ChangePriorityTaskInput request;

    public ChangePriorityTaskCmd(TaskService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        if(!validate()){
            throw new EntityNotFoundException(Constants.ERRORS.ERROR_404);
        }

        changePriority();
    }

    private Boolean validate() {
        return service.exist(request.getTaskId());
    }

    private void changePriority() {
        try {
            change();
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500);

        }
    }

    private void change() {
        service.changePriorityTask(request.getTaskId(), request.getIsPriority());
    }
}
