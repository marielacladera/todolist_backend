package com.prueba.todolist.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.prueba.todolist.constants.Constants;
import com.prueba.todolist.input.UpdateRequestTaskInput;
import com.prueba.todolist.service.TaskService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class UpdateTaskCmd  implements BusinessLogicCommand {

    private TaskService service;

    @Setter
    private UpdateRequestTaskInput request;

    @Getter
    private Integer response;

    public UpdateTaskCmd(TaskService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        if(!validate()){
            throw  new ResponseStatusException(
                    HttpStatus.NOT_FOUND, Constants.ERRORS.ERROR_404);
        }

        update();
    }

    private Boolean validate() {
        return service.exist(request.getTaskId());
    }

    private void update() {
        try{
            response = updateTask();
        }   catch (Exception e) {
            throw  new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500);
        }
    }

    private Integer updateTask() {
        return service.updateTask(request.getTitle(), request.getDescription(), request.getLimitedDateToTask(), request.getIsPriority(), request.getTaskId());
    }

}
