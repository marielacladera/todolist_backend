package com.prueba.todolist.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.prueba.todolist.constants.Constants;
import com.prueba.todolist.domain.Task;
import com.prueba.todolist.service.TaskService;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class ListTaskCmd implements BusinessLogicCommand {

    private TaskService service;

    public ListTaskCmd(TaskService service) {
        this.service = service;
    }

    @Getter
    private List<Task> response;

    @Override
    public void execute() {
        list();
    }

    private void list() {
        try{
            response = listTask();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private List<Task> listTask() {
        return service.listTask();
    }
    
}
