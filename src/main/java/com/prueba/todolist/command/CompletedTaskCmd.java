package com.prueba.todolist.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.prueba.todolist.constants.Constants;
import com.prueba.todolist.service.TaskService;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class CompletedTaskCmd implements BusinessLogicCommand {
    private TaskService service;

    @Setter
    private Integer taskId;

    public CompletedTaskCmd(TaskService service) {
        this.service = service;
    }

    @Override
    public void execute() {

        if(!validate()){
            throw  new ResponseStatusException(
                    HttpStatus.NOT_FOUND, Constants.ERRORS.ERROR_404);
        }

        completeTask();
    }

    private Boolean validate() {
        return service.exist(taskId);
    }

    private void completeTask() {
        try {
            complete();
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500);

        }
    }

    private void complete() {
        service.completeTask(taskId);
    }
}
