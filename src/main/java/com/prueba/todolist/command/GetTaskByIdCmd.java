package com.prueba.todolist.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.prueba.todolist.constants.Constants;
import com.prueba.todolist.domain.Task;
import com.prueba.todolist.service.TaskService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class GetTaskByIdCmd implements BusinessLogicCommand {
    private TaskService service;

    @Setter
    private Integer taskId;

    @Getter
    private Task response;

    public GetTaskByIdCmd(TaskService service) {
        this.service = service;
    }

    @Override
    public void execute() {
        Task  r = getTaskById();
        if(r == null) {
            throw  new ResponseStatusException(
                    HttpStatus.NOT_FOUND, Constants.ERRORS.ERROR_404);
        }
        response = r;
    }

    private Task getTaskById() {
        return service.getTaskById(taskId);
    }

}
