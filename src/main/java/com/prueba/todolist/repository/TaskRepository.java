package com.prueba.todolist.repository;

import com.prueba.todolist.domain.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Repository
public interface TaskRepository extends JpaRepository<Task, Integer> {

    @Modifying
    @Transactional
    @Query("update Task t set t.isRemoved = true where t.taskId = :taskId")
    void removeTask(Integer taskId);

    @Modifying
    @Transactional
    @Query("update Task t set t.isPriority = :state where t.taskId = :taskId")
    void changePriorityTask(Integer taskId, Boolean state);

    @Modifying
    @Transactional
    @Query("update Task t set t.isCompleted = true where t.taskId = :taskId")
    void completeTask(Integer taskId);

    @Query("select t from Task t where t.isRemoved = false and t.isCompleted = false")
    List<Task> listAll();

    @Modifying
    @Transactional
    @Query("update Task t set t.title = :title, t.description= :description, t.limitedDateToTask = :limitedDateToTask, t.isPriority= :isPriority, t.updatedDate= :updateDate where t.taskId = :taskId and t.isCompleted = false and t.isRemoved = false")
    Integer updateTask(String title, String description, LocalDateTime limitedDateToTask, Boolean isPriority, Integer taskId, LocalDateTime updateDate);
}
