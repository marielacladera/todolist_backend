package com.prueba.todolist.builders;

import com.prueba.todolist.domain.Task;
import com.prueba.todolist.input.RequestTaskInput;

import java.time.LocalDateTime;

/**
 * @author Mariela Cladera M.
 */
public class ComposeTaskBuilder {
    public static Task composeTask(RequestTaskInput task) {
        Task instance = new Task();

        instance.setTitle(task.getTitle());
        instance.setDescription(task.getDescription());
        instance.setIsPriority(task.getIsPriority());
        instance.setCreatedDate(LocalDateTime.now());
        instance.setUpdatedDate(LocalDateTime.now());
        instance.setLimitedDateToTask(task.getLimitedDateToTask());
        instance.setIsCompleted(false);
        instance.setIsRemoved(false);

        return instance;
    }
}
