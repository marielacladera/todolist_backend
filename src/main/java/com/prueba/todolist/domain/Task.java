package com.prueba.todolist.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
@Entity
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer taskId;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private String description;

    private LocalDateTime createdDate;

    private LocalDateTime updatedDate;

    private LocalDateTime limitedDateToTask;

    private Boolean isPriority;

    private Boolean isCompleted;

    private Boolean isRemoved;
}
