package com.prueba.todolist.constants;

/**
 * @author Mariela Cladera M.
 */
public final class Constants {

    private Constants() {
    }

    public static class ERRORS {
        public static final String ERROR_500 = "Internal service error";
        public static final String ERROR_404 = "ID not found";
    }

    public static class TASKS {
        public static final String TASKS_PATH = "/tasks";
        public static final String TASKS_BY_ID_PATH = "/tasks/{id}";

        public static final String TASKS_COMPLETE = "/tasks/complete/{id}";

        public static final String TASKS_DELETE = "/tasks/delete/{id}";

        public static final String TASKS_PRIORITY = "/tasks/priority";
    }

    public static class TAGS {
        public static final String TASK_TAG = "Controllers of Task";
    }
}
