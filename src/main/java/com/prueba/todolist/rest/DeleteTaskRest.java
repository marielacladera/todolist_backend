package com.prueba.todolist.rest;

import com.prueba.todolist.command.DeleteTaskCmd;
import com.prueba.todolist.constants.Constants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS.TASK_TAG)
@RestController
@RequestScope
public class DeleteTaskRest {
    private DeleteTaskCmd commander;

    public DeleteTaskRest(DeleteTaskCmd commander) {
        this.commander = commander;
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "500", description = "Oh God, we have a problem with the server", content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found the task, please change the ID", content = @Content),
            @ApiResponse(responseCode = "400", description = "Oh God, your request has any errors", content = @Content)
    })
    @Operation(summary = "Delete a task by ID")
    @PutMapping(value = Constants.TASKS.TASKS_DELETE)
    public ResponseEntity<Void> deleteTask(@PathVariable("id") Integer id) {
        commander.setTaskId(id);
        commander.execute();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
