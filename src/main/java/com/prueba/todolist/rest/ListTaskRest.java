package com.prueba.todolist.rest;

import com.prueba.todolist.command.ListTaskCmd;
import com.prueba.todolist.constants.Constants;
import com.prueba.todolist.domain.Task;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS.TASK_TAG)
@RestController
@RequestScope
public class ListTaskRest {

    private ListTaskCmd commander;

    public ListTaskRest(ListTaskCmd commander) {
        this.commander = commander;
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "500", description = "Oh God, we have a problem with the server", content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found the task, please change the ID", content = @Content),
            @ApiResponse(responseCode = "400", description = "Oh God, your request has any errors", content = @Content)
    })
    @Operation(summary = "List all tasks")
    @GetMapping(value = Constants.TASKS.TASKS_PATH)
    public ResponseEntity<List<Task>> lisTask() {
        commander.execute();
        return new ResponseEntity<List<Task>>(commander.getResponse(), HttpStatus.OK);
    }
 }
