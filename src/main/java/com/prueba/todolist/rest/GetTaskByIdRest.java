package com.prueba.todolist.rest;

import com.prueba.todolist.command.GetTaskByIdCmd;
import com.prueba.todolist.constants.Constants;
import com.prueba.todolist.domain.Task;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS.TASK_TAG)
@RestController
@RequestScope
public class GetTaskByIdRest {
    private GetTaskByIdCmd commander;

    public GetTaskByIdRest(GetTaskByIdCmd commander) {
        this.commander = commander;
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "500", description = "Oh God, we have a problem with the server", content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found the task, please change the ID", content = @Content),
            @ApiResponse(responseCode = "400", description = "Oh God, your request has any errors", content = @Content)
    })
    @Operation(summary = "Get a task by ID")
    @GetMapping(value = Constants.TASKS.TASKS_BY_ID_PATH)
    public ResponseEntity<Task> getTaskById(@PathVariable("id") Integer id) {
        commander.setTaskId(id);
        commander.execute();
        return new ResponseEntity<>(commander.getResponse(), HttpStatus.OK);
    }
}
