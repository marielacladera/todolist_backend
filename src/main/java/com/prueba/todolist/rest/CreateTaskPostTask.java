package com.prueba.todolist.rest;

import com.prueba.todolist.command.CreateTaskCmd;
import com.prueba.todolist.constants.Constants;
import com.prueba.todolist.domain.Task;
import com.prueba.todolist.input.RequestTaskInput;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@Tag(name = Constants.TAGS.TASK_TAG)
@RestController
@RequestScope
public class CreateTaskPostTask {

    private CreateTaskCmd commander;

    public CreateTaskPostTask(CreateTaskCmd commander) {
        this.commander = commander;
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "500", description = "Oh God, we have a problem with the server", content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found the task, please change the ID", content = @Content),
            @ApiResponse(responseCode = "400", description = "Oh God, your request has any errors", content = @Content)
    })
    @Operation(summary = "Create a new task")
    @PostMapping(value = Constants.TASKS.TASKS_PATH)
    public ResponseEntity<Task> createTask(@Valid @RequestBody RequestTaskInput request) {
        commander.setRequest(request);
        commander.execute();
        return new ResponseEntity<Task>(commander.getResponse(), HttpStatus.CREATED);
    }
}
