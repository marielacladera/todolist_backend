package com.prueba.todolist.service;

import com.prueba.todolist.domain.Task;
import com.prueba.todolist.repository.TaskRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Service
public class TaskService {

    private TaskRepository repository;

    public TaskService(TaskRepository repository) {
        this.repository = repository;
    }

    public Task createTask(Task task) {
        return repository.save(task);
    }

    public Task updateTask(Task task) {
        return repository.save(task);
    }

    public List<Task> listTask() {
        return repository.listAll();
    }

    public Task getTaskById(Integer taskId) {
        return repository.findById(taskId).orElse(null);
    }

    public Boolean exist(Integer taskId) {
        return repository.existsById(taskId);
    }

    public void deleteTask(Integer taskId) {
        repository.removeTask(taskId);
    }

    public void completeTask(Integer taskId) {
        repository.completeTask(taskId);
    }

    public void changePriorityTask(Integer taskId, Boolean state) {
        repository.changePriorityTask(taskId, state);
    }

    public Integer updateTask(String title, String description, LocalDateTime limitedDateToTask, Boolean isPriority, Integer taskId) {
        return repository.updateTask(title, description, limitedDateToTask, isPriority, taskId, LocalDateTime.now());
    }
}
