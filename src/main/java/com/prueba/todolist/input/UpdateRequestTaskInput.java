package com.prueba.todolist.input;

import lombok.Getter;

import java.time.LocalDateTime;

/**
 * @author Mariela Cladera M.
 */
@Getter
public class UpdateRequestTaskInput {

    private Integer taskId;

    private String title;

    private String description;

    private LocalDateTime limitedDateToTask;

    private Boolean isPriority;

}
