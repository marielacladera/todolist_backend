package com.prueba.todolist.input;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;

/**
 * @author Mariela Cladera M.
 */
@Getter
public class ChangePriorityTaskInput {

    @NotNull
    private Integer taskId;

    @NotNull
    private Boolean isPriority;

}