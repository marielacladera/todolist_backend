package com.prueba.todolist.input;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;

import java.time.LocalDateTime;

/**
 * @author Mariela Cladera M.
 */
@Getter
public class RequestTaskInput {

    @NotNull
    private String title;

    @NotNull
    private String description;

    @NotNull
    private Boolean isPriority;

    private LocalDateTime limitedDateToTask;
}
